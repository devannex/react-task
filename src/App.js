import React from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Registration from './components/Registration';
import Login from './components/Login';
import Home from './components/Home';

function App() {
  return (
    < >
      <Router>
        <Switch>
          <Route path="/Registration" exact component={Registration} />
          <Route path="/login" component={Login} />
          <Route path ="/home" component ={Home} />
          <Redirect to="/home" />
        </Switch>
      </Router>
    </>
  );
}

export default App;
