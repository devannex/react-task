import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { todoaction, gettodo } from './action/index'
import '../assets/home.scss'
export class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            localStoragedata: localStorage.getItem("logindetails"),
            fields: {},
            errors: {},
            items: []
        }
    }


    componentDidMount() {
        this.props.gettodo();

    }
    logout(e) {
        // localStorage.removeItem('logindetails')
        e.preventDefault();
        localStorage.setItem('logindetails', "")
        window.location.reload(true);
        console.log("logout");
        // return <Redirect to ='/login' />

    }
    handleValidationTodo() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields["todo_title"]) {
            formIsValid = false;
            errors["todo_title"] = "Cannot be empty Todo ";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }
    tododsubmit(e) {


        if (this.handleValidationTodo()) {
            const tododata = this.state.fields.todo_title;
            this.props.todoaction(tododata);
        }
        // const newitems = this.props.dataresponse.message;
        // this.setState({ items: this.state.items.concat(newitems) })
        // console.log(this.state.items , "';[koiejfoihdfdiuhfdfhuijhijh")
        e.preventDefault();

    }

    handleChange(field, e) {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }
    render() {
        if (!this.state.localStoragedata) {
            console.log("have a fun with reactjs ");
            return <Redirect to="/login" />
        }

        let isshowtodolist = this.props.dataresponse.message;
        console.log(isshowtodolist, "<<<<<<<<<<<<data>>>>>");

        return (
            <div className="text-center mt-5 home-page">
                <div className="heading">
                    <h1>Todo List !</h1>
                    <button onClick={this.logout}>logout</button>
                </div>
                <form onSubmit={this.tododsubmit.bind(this)}>
                    <input type="text" onChange={this.handleChange.bind(this, "todo_title")} value={this.state.fields["todo_title"]}
                        placeholder="Enter you todo" />

                    <button>Add</button><br /><br />
                    <span className="error" style={{ color: "red" }}>
                        {this.state.errors["todo_title"]}
                    </span>
                </form>
                <div className="items">
                    <ul>
                        {isshowtodolist.map((value, index) => <li key={index -1}>{value}</li>)}
                        {/* {this.state.items.map((value, index) => <li key={index -1}>{value}</li>)} */}

                    </ul>

                </div>
                {/* <h2>{this.props.listdata}</h2> */}

            </div>
        )
    }
}
const mapStateToProps = (state) => ({
    listdata: state.data,
    dataresponse: state.posttodo

});

const mapDispatchToProps = {
    gettodo: gettodo,
    todoaction: todoaction
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
