import {put ,takeLatest ,all} from 'redux-saga/effects';


function* feacthtodo(){

    const api = yield  fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json(),);
    console.log(api.title +" Saga");
    
    yield put ({type:"TODO_RECEIVED" , apidata:api,});
}

function* actionWatcher(){
    yield takeLatest ('GET_TODO' ,feacthtodo)
}
export default function* rootsaga(){
    yield all([
        actionWatcher(),
    ]);
}