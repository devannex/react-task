import { takeEvery, call, put } from 'redux-saga/effects';
import Axios from 'axios';


export default function* PostTodosaga() {
    yield takeEvery("TODOACTION", tododata);
}
export async function posthttpcall(method, url, data, headers = {}) {
    return await Axios({
        method,
        url,
        data,
        headers: {
            "Content-Type": "application/json",
            ...headers
        }
    });
}

export function* tododata(action){
    try{
        const posttododatasaga = yield call(
            posthttpcall,
            "post",
            "https://jsonplaceholder.typicode.com/posts",
            {
                // "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                title:action.payload,
                "id":"1",
                "UserId":"1",
                "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"

            }
        );
        console.log(posttododatasaga.data ,"  >>>>>>> Response");
        if(posttododatasaga.status===201){
            yield put({type:"POST_TODO_SUCCESS" , posttododatasaga:posttododatasaga.data.title});
            console.log("<<<<data sucessfull>>>",posttododatasaga.data.title);
        }else{
            yield put({type:"POST_TODO_FAILED" , posttododatasaga:posttododatasaga.data.title});
            console.log("Failed  fdkfkk" , posttododatasaga.data);
        }
    }
    catch(error){
        yield put ({type:"POST_TODO_FAILED" , posttododatasaga:"Network Error! Please try again after sometime"});
        console.log(error);
    }
}