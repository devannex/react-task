import {createStore , applyMiddleware ,compose} from 'redux';
import  createSagaMiddleware from 'redux-saga';
import reducer from '../reducers/index.js';
import PostTodosaga from "../saga/posttodo";
import rootsaga from '../saga/gettodoitems'

const sagaMiddleware = createSagaMiddleware();

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const   store = createStore(reducer, storeEnhancers(applyMiddleware(sagaMiddleware))
 );
 sagaMiddleware.run(rootsaga);
 sagaMiddleware.run(PostTodosaga);
 export default store;