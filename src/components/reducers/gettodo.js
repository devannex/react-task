const getreducer = (state = {}, action) => {
    switch (action.type) {
        case 'GET_TODO':
            return {
                ...state,
                loading: true
            };
        case 'TODO_RECEIVED':
            return {
                ...state,
                data: action.apidata.title,
                loading: false
            };
        default:
            return state;
    }
}
export default getreducer;