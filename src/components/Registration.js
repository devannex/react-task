import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Redirect } from 'react-router-dom';
class Registration extends Component {
    StordData;
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            fields: {},
            errors: {},
            redirect: null

        }
        // this.handlesubmit = this.handlesubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }
    handleValidation() {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
        if (!fields["username"]) {
            formIsValid = false;
            errors["username"] = "Cannot be empty Username ";
        }

        if (!fields["password"]) {
            formIsValid = false;
            errors["password"] = "Cannot be empty Password ";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChange = (field, e) => {
        let fields = this.state.fields;
        fields[field] = e.target.value;
        this.setState({ fields });
    }
    handlesubmit(e) {
        if (this.handleValidation()) {
            localStorage.setItem('usersname', JSON.stringify(this.state.fields));
            this.setState({ redirect: "/login" })
        }
        e.preventDefault()
    }


    render() {
        if(this.state.redirect){
            return <Redirect to ={this.state.redirect} />
        }
        return (
            <div className="container">
                <div className="mr-auto ml-auto reg-form">
                    <h1 className="text-center md-5">Registration Page</h1>
                    <form onSubmit={this.handlesubmit.bind(this)}>
                        <div className="form-group">
                            <label>Username :-</label>
                            <input type="text" name="username" className="form-control"
                                onChange={this.handleChange.bind(this, "username")} value={this.state.fields["username"]} placeholder="UserName" />
                            <span className="error" style={{ color: "red" }}>
                                {this.state.errors["username"]}
                            </span>
                        </div>

                        <div className="form-group">
                            <label>Password :-</label>
                            <input type="text" name="password" className="form-control"
                                onChange={this.handleChange.bind(this, "password")} value={this.state.fields["password"]} placeholder="Password" />
                            <span className="error" style={{ color: "red" }}>
                                {this.state.errors["password"]}
                            </span>
                        </div>
                        <div className="row">
                            <div className="col-6"><button type="submit" className="btn btn-primary">Submit</button></div>
                            <div className="col-6 text-right"> <a className="login-btn" href="/login">Login</a></div>
                        </div>
                    </form>
                    {/* {this.StordData.username} */}
                    {/* <ol>
                    <li>
                    {localStorage.Username}
                    </li>
                </ol> */}
                </div>
            </div>

        );
    }
}

export default Registration;