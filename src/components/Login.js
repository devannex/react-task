import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
export class Login extends Component {
    constructor() {
        super();
        this.state = {
            mydata: "",
            logusername: "",
            logpassword: "",
            redirect:null
        };
        this.onSubmitdata = this.onSubmitdata.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    componentDidMount() {
        // onSubmitdata(e){
        // var logusername = this.state.logusername
        var th = this;
        const devdata = JSON.parse(localStorage.getItem('usersname'));
        th.setState({
             mydata: devdata,
        })

        // }

    }
    onSubmitdata(e) {
        // var username = this.state.logusername
        
        if (this.state.logusername === this.state.mydata.username && this.state.logpassword === this.state.mydata.password) {
            console.log("login " + this.state.logusername)
            
            alert("Lgoin sucessFull ");
            
            localStorage.setItem('logindetails', JSON.stringify(this.state.mydata.username));
        this.setState({redirect:"/home"})


        }
        else {
            alert( "Please Enter Valid UserName & Password");
        }
        e.preventDefault();

    }

    render() {
        if(this.state.redirect){
            return <Redirect to ={this.state.redirect} />
        }
        return (

            <div className="container">
                <div className="mr-auto ml-auto reg-form">
                    <h1 className="text-center">Login Page</h1>
                    <form onSubmit={this.onSubmitdata.bind(this)}>
                        <div className="form-group">
                            <label>Username :-</label>
                            <input type="text" placeholder="UserName" onChange={this.handleChange} name="logusername"
                                value={this.state.logusername} className="form-control" />
                        </div>
                        <div className="form-group">
                            <label>password :-</label>
                            <input type="text" placeholder="Password" onChange={this.handleChange} name="logpassword"
                                value={this.state.logpassword} className="form-control" />
                        </div>

                        <div className="row">
                            <div className="col-6">
                                <button type="submit" className="btn btn-primary btn-block" >login</button>
                            </div>
                            <div className="col-6 text-right"> <a href="/Registration">Registration</a></div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login
